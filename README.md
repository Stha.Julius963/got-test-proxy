## Run App

`npm install`

## Nodemon

`npm install -g nodemon`

## Start App

`npm start`

The app runs on `localhost:5000` in production

## Proxy

on `localhost:5000/`, the app runs without proxy
on `localhost:5000/proxy`, the app runs with proxy

## Packages

```
"dependencies": {
    "express": "^4.17.1",
    "got": "^11.8.2",
    "hpagent": "^0.1.2",
    "tunnel": "0.0.6"
  }
```

## Note

Got error while using tunnel so hpagent was used for proxy
