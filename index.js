const got = require("got");
const yenv = require("yenv");
const envs = require("env-smart");
const tunnel = require("tunnel");
const { HttpsProxyAgent } = require("hpagent");
const express = require("express");
const { response } = require("express");
const { Tunnel } = require("node-tunnel");
const app = express();
const env = yenv("./config/config.yaml", { env: "development" });

//read environment variable
const http_proxy = envs.load().http_proxy;
//read config.yaml
const y_http_proxy = env.http_proxy;

//localhost port
const PORT = env.PORT;

const server = app.listen(
  PORT,
  console.log(`Server is running in PORT ${PORT}`)
);

//handling the unhandled promise rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
  //closing the server
  server.close(() => process.exit(1));
});

//without proxy
app.get("/", (req, res) => {
  (async () => {
    try {
      const response = await got("https://sindresorhus.com");
      console.log("statusCode:", response.statusCode);
      res.send(response.body);
    } catch (error) {
      console.log(error.response.body);
    }
  })();
});

// with proxy
app.get("/proxy", (req, res) => {
  (async () => {
    try {
      if (typeof http_proxy !== "undefined" && !http_proxy) {
        const response = await got("https://gorest.co.in/public/v1/users", {
          agent: {
            https: new HttpsProxyAgent({
              keepAlive: true,
              keepAliveMsecs: 1000,
              maxSockets: 256,
              maxFreeSockets: 256,
              scheduling: "lifo",
              proxy: `${http_proxy}`,
            }),
          },
        });
        console.log("OS HTTP_PROXY Environment variable :", `${http_proxy}`);
        res.send(response.body);
      } else if (typeof y_http_proxy !== "undefined" && y_http_proxy) {
        const response = await got("https://gorest.co.in/public/v1/users", {
          agent: {
            https: new HttpsProxyAgent({
              keepAlive: true,
              keepAliveMsecs: 1000,
              maxSockets: 256,
              maxFreeSockets: 256,
              scheduling: "lifo",
              proxy: `${y_http_proxy}`,
            }),
          },
        });

        console.log(
          "Reading http_proxy from Config File:",
          `${env.http_proxy}`
        );
        res.send(response.body);
      }
    } catch (error) {
      res.send(
        "Proxy setting are not present in environment variable and config file "
      );
      console.log(
        "Proxy setting are not present in environment variable and config file "
      );
    }
  })();
});
//with tunnel
app.get("/tunnel", (req, res) => {
  (async () => {
    try {
    
      const response = got("https://sindresorhus.com", {
        agent: {
          https: tunnel.httpsOverHttps({
            proxy: {
              host: `http://18.189.184.110:3128`,
            },
          }),
        },
      });
      res.status(200).json({
        sucess: true,
        data: response.body,
      });

  
    } catch (error) {
      // console.log(error.response.body);
    }
  })();
});
